console.log("Hello World");

// Arithmetic Operators

let x = 3;
let y = 10;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of the modulo operator: " + remainder);

// Assignment Operator ( = )
// Basic Assignment Operator ( = )
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

// concatenation

// Multiple Operators and Parentheses

// MDAS ( Multiplication and Division, Addition and Subtraction)
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

let z = 1;
//pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment (z): " + z);

//post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment (z): " + z);

//pre-decrement
decrement = --z;
console.log("Result of pre-increment: " + decrement);
console.log("Result of pre-increment (z): " + z);

//post-decrement

decrementp = z--;
console.log("Result of post-increment: " + decrement);
console.log("Result of post-increment (z): " + z);

console.log("-------------------------------")
// Type coercion
/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/

// String and Number
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Bolean and Number
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

let numF = false + 1;
console.log(numF);
console.log(typeof numF);

// Equality operator ( == )
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = "juan";
let pedro='juan';

console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == "1"); // true
console.log(1 == "one"); // false
console.log("juan" == "juan"); // true
console.log(juan == "juan"); // true
console.log(pedro == 'juan'); // true

console.log("---------------------------");

// Inequality operator ( != )
// Not equal
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != "1"); // false
console.log("juan" != "juan"); // false
console.log("juan" != juan); // false

// Strict Equality Operator ( === )
/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log("-----------------------");
console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === "1"); // false
console.log(0 === false); // false
console.log("juan" === "juan"); // true
console.log(juan === "juan"); // true

// Strict Inequality operator.
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/

console.log("-----------------------");
console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== "1"); // true
console.log(0 !== false); // true
console.log("juan" !== "juan"); // false
console.log(juan !== "juan"); // false

// Relational Operators ( >, <, >=, <= )
//Some comparison operators check whether one value is greater or less than to the other value.

console.log("-----------------------");

let a = 50;
let b = 65;

let isGreaterThan = a > b; // false
let isLessThan = a < b; // true
let isGTorEqual = a >= b; // false
let isLTorEqual = a <= b; // true

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// Logical Operators ( && - AND, || - OR, ! - NOT )

console.log("-----------------------");

// Logical AND Operator
let isLegalAge = true;
let isRegistered = true;
let isMarried = false;

let isLegalAgeAndisRegistered = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + isLegalAgeAndisRegistered);

let allCondition = isLegalAge && isRegistered && isMarried;
console.log("Result of logical AND operator: " + allCondition);

// Logical OR Operator
let someRequirements = isLegalAge || isMarried;
console.log("Result of logical OR operator: " + someRequirements);

// Logical NOT Operator
let someRequirementsNotMet = !isRegistered; // false
console.log("Result of logical NOT operator: " + someRequirementsNotMet);